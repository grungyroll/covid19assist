
let name = localStorage.getItem('name')
let sex = localStorage.getItem('sex')
let age = localStorage.getItem('age')
let area = localStorage.getItem('location')
let emergencySymptoms1 = localStorage.getItem('emergencySymptoms1')
let emergencySymptoms2 = localStorage.getItem('emergencySymptoms2')
let emergencySymptoms3 = localStorage.getItem('emergencySymptoms3')
let travelHistory = localStorage.getItem('travelHistory')
let expo1 = localStorage.getItem('expo1')
let expo2 = localStorage.getItem('expo2')
let expo3 = localStorage.getItem('expo3')
let expo4 = localStorage.getItem('expo4')
let expo5 = localStorage.getItem('expo5')
let symptoms1 = localStorage.getItem('symptoms1')
let symptoms2 = localStorage.getItem('symptoms2')
let symptoms3 = localStorage.getItem('symptoms3')
let symptoms4 = localStorage.getItem('symptoms4')
let symptoms5 = localStorage.getItem('symptoms5')
let symptoms6 = localStorage.getItem('symptoms6')
let symptoms7 = localStorage.getItem('symptoms7')
let symptoms8 = localStorage.getItem('symptoms8')
let symptoms9 = localStorage.getItem('symptoms9')
let symptoms10 = localStorage.getItem('symptoms10')
let symptoms11 = localStorage.getItem('symptoms11')
let assessmentnum = localStorage.getItem('assessmentnum')

console.log(name)
console.log(age)

// NAME
if(name === null) {
	document.querySelector("#profileName").innerHTML = "Hello, Guest"
} else {
	document.querySelector("#profileName").innerHTML = "Hello, " + name;
}

// AGE
if(age === null) {
	document.querySelector("#age").innerHTML = "";
} else {
	document.querySelector("#age").innerHTML = age;
}

// SEX

if(sex == 1) {
	document.querySelector("#sex").innerHTML = "Male";
} else {
	document.querySelector("#sex").innerHTML = "Female";
}


// AREA
if(area === null) {
	document.querySelector("#location").innerHTML = "";
} else {
	document.querySelector("#location").innerHTML = area;
}

if(travelHistory == 1) {
	document.querySelector("#travelHistory").innerHTML = "With travel history in the past 14 days";
} else {
	document.querySelector("#travelHistory").innerHTML = "No travel history in the past 14 days";
}

// EMERGENCY SYMPTOMS
// if(emergencySymptoms3 == "None") {
// 	document.querySelector("#emergencySymptoms3").innerHTML = "None";
// } else {
// 	document.querySelector("#emergencySymptoms1").innerHTML = emergencySymptoms1;
// 	document.querySelector("#emergencySymptoms2").innerHTML = emergencySymptoms2;
// 	document.querySelector("#emergencySymptoms3").innerHTML = emergencySymptoms3;
// }

// SYMPTOMS
if(symptoms11 == "None") {
	//document.querySelector("#emergencySymptoms").innerHTML = "None";
	document.querySelector("#symptoms11").innerHTML = symptoms11;
} else {
	document.querySelector("#symptoms1").innerHTML = symptoms1;
	document.querySelector("#symptoms2").innerHTML = symptoms2;
	document.querySelector("#symptoms3").innerHTML = symptoms3;
	document.querySelector("#symptoms4").innerHTML = symptoms4;
	document.querySelector("#symptoms5").innerHTML = symptoms5;
	document.querySelector("#symptoms6").innerHTML = symptoms6;
	document.querySelector("#symptoms7").innerHTML = symptoms7;
	document.querySelector("#symptoms8").innerHTML = symptoms8;
	document.querySelector("#symptoms9").innerHTML = symptoms9;
	document.querySelector("#symptoms10").innerHTML = symptoms10;
	document.querySelector("#symptoms11").innerHTML = symptoms11;
}

// EXPOSURE
if(expo5 == "None") {
	document.querySelector("#emergencySymptoms").innerHTML = "None";
} else {
	document.querySelector("#expo1").innerHTML = expo1;
	document.querySelector("#expo2").innerHTML = expo2;
	document.querySelector("#expo3").innerHTML = expo3;
	document.querySelector("#expo4").innerHTML = expo4;
	document.querySelector("#expo5").innerHTML = expo5;
}

const finalassessment = ["Not PUI or PUM - Person is neither person under Investigation nor Person under Monitoring",
"PUM - Person Under Monitoring", "PUI - Person Under Investigation"]
document.querySelector("#assessment").innerHTML = finalassessment[assessmentnum];
console.log("assessmentnum" + assessmentnum)
if(assessmentnum==2){
	if(symptoms3!=null){
		document.querySelector("#mildOrSevere").innerHTML = "*with Severe Manifestation";
		document.querySelector("#typePui").innerHTML = "*Need for swab testing";
	} 
	else if(assessmentnum==2&&age>=60){
		document.querySelector("#mildOrSevere").innerHTML = "*with Mild Manifestation";
		document.querySelector("#typePui").innerHTML = "*Need for swab testing";	
	} else{
		document.querySelector("#mildOrSevere").innerHTML = "*with Mild Manifestation";
		document.querySelector("#typePui").innerHTML = "*Need Home quarantine";
	}
} else{
	document.querySelector("#mildOrSevere").innerHTML = "";
		document.querySelector("#typePui").innerHTML = "";
}
console.log("symptoms3: "+symptoms3);